class CaskRepair < Formula
  desc "Script to quickly repair outdated/broken Casks from homebrew-cask"
  homepage "https://bitbucket.org/Sticklerm3/brue-scripts/src/master/"
  url "https://Sticklerm3@bitbucket.org/Sticklerm3/brue-scripts.git"
  version "0.0.1"

  depends_on "hub"
  depends_on "hr" => :recommended

  def install
    bin.install "cask-repair"
  end
end

# Sticklerm3 Motley-brue

## How do I install these formulae?

`brew install sticklerm3/motley-brue/<formula>`

Or `brew tap sticklerm3/motley-brue` and then `brew install <formula>`.

Or install via URL (which will not receive updates):

```
brew install https://raw.githubusercontent.com/sticklerm3/homebrew-motley-brue/master/Formula/<formula>.rb
```

## Documentation

`brew help`, `man brew` or check [Homebrew's documentation](https://docs.brew.sh).
